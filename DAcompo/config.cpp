class CfgPatches {
	class DAcompo {
		units[] = {};
		weapons[] = {};
		requiredVersion = 1;
		requiredAddons[] = {};
		version = "0.1";
		versionStr = "0.1";
		versionDesc = "Direct Action: Compositions";
		versionAr[] = {"0.1"};
		author[] = {"Ben"};
	};
};

class CfgMods {
	class DAcompo {
		dir="DAcompo";
		name="Direct Action: Compositions";
		picture="";
	};
};

class DAcompo {
	class ALTIS_airbase {
		#include "airbase.Altis\mission.sqm"
	};
	class MALDEN_small {
		#include "smallOutpost.Malden\mission.sqm"
	};
	class PREI_KHMAOCH_LUONG_jungle {
		#include "jungle.prei_khmaoch_luong\mission.sqm"
	};
	class TANOA_tuvanaka {
		#include "tuvanaka.Tanoa\mission.sqm"
	};
};
